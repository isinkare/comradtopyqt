# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'cmrd/cmrd.ui'
#
# Created by: PyQt5 UI code generator 5.12.3
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(800, 600)
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.gridLayout = QtWidgets.QGridLayout(self.centralwidget)
        self.gridLayout.setObjectName("gridLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.CLabel = CLabel(self.centralwidget)
        self.CLabel.setTextFormat(QtCore.Qt.PlainText)
        self.CLabel.setProperty("precision", 0)
        self.CLabel.setProperty("channel", "japc://isinkareDevice/Acquisition#float")
        self.CLabel.setObjectName("CLabel")
        self.horizontalLayout.addWidget(self.CLabel)
        self.lineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEdit.setObjectName("lineEdit")
        self.horizontalLayout.addWidget(self.lineEdit)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.CScrollingPlot = CScrollingPlot(self.centralwidget)
        self.CScrollingPlot.setProperty("timeSpan", 10.0)
        self.CScrollingPlot.setProperty("curves", ['{"channel": "japc://isinkareDevice/Acquisition#float", "style": "Line Graph", "layer": "", "name": "", "color": "red", "line_style": 1, "line_width": 2, "symbol": null, "symbol_size": 5}'])
        self.CScrollingPlot.setObjectName("CScrollingPlot")
        self.verticalLayout.addWidget(self.CScrollingPlot)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setObjectName("label")
        self.horizontalLayout_2.addWidget(self.label)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.gridLayout.addLayout(self.verticalLayout, 0, 0, 1, 1)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 800, 20))
        self.menubar.setObjectName("menubar")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)

        self.retranslateUi(MainWindow)
        self.lineEdit.textChanged['QString'].connect(self.label.setText)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "TextLabel"))
from comrad.widgets.graphs import CScrollingPlot
from comrad.widgets.indicators import CLabel
